* misc.tkinter-password-manager
A password manager, allowing the user to store and retrieve passwords for
different websites and programs.

The user first enters a master password which is used to encrypt the bank of
passwords. This master password is required to access and modify the bank. Upon
accessing the bank, the user is able to create entries containing a description
of what the password is for and the password itself. When the user needs a
password, they simply browse for the description.