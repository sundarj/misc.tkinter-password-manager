import tkinter as tk
import tkinter.messagebox as messagebox


def main():
    root = tk.Tk()
    master_password = None
    items = []
    master_password_label = tk.Label(root, text="Master password:")
    master_password_entry = tk.Entry(root, show="•")
    master_password_entry.focus()
    def open_create_password_window():
        win = tk.Toplevel(root)
        description_label = tk.Label(win, text="Description:")
        description_entry = tk.Entry(win)
        description_entry.focus()
        username_label = tk.Label(win, text="Username:")
        username_entry = tk.Entry(win)
        password_label = tk.Label(win, text="Password:")
        password_entry = tk.Entry(win, show="•")
        def add_item_to_bank():
            description = description_entry.get()
            if not description:
                messagebox.showerror("Error", "Description is empty")
            else:
                username = username_entry.get()
                if not username:
                    messagebox.showerror("Error", "Username is empty")
                else:
                    password = password_entry.get()
                    if not password:
                        messagebox.showerror("Error", "Password is empty")
                    else:
                        items.append({
                            "description": description,
                            "username": username,
                            "password": password
                        })
                        win.destroy()
        submit_button = tk.Button(win, text="Add to bank", command=add_item_to_bank)
        description_label.pack()
        description_entry.pack()
        username_label.pack()
        username_entry.pack()
        password_label.pack()
        password_entry.pack()
        submit_button.pack()
    def render_items(frame):
        for child in frame.winfo_children():
            child.destroy()
        for index, item in enumerate(items):
            description_label = tk.Label(frame, text=item["description"])
            username_label = tk.Label(frame, text=item["username"])
            def copy_password(password):
                frame.clipboard_clear()
                frame.clipboard_append(password)
            # lambda item=item is a common trick for making the closure store
            # the current value of the item variable instead of a reference
            # to the variable, which is accessed when the lambda is called
            # taking on the value of the last iteration
            copy_password_button = tk.Button(frame, text="Copy password", command=lambda item=item: copy_password(item["password"]))
            description_label.grid(row=index, column=0)
            username_label.grid(row=index, column=1)
            copy_password_button.grid(row=index, column=2)
        frame.after(1000, lambda: render_items(frame))
    def open_password_bank_window():
        if not master_password:
            messagebox.showerror("Error", "Master password has not been set")
        else:
            mp = master_password_entry.get()
            if not mp:
                messagebox.showerror("Error", "Master password is empty")
            else:
                if mp != master_password:
                    messagebox.showerror("Error", "Master password is incorrect")
                else:
                    win = tk.Toplevel(root)
                    create_password_button = tk.Button(win, text="Create password", command=open_create_password_window)
                    create_password_button.pack()
                    frame = tk.Frame(win)
                    frame.pack()
                    render_items(frame)
    open_bank_button = tk.Button(root, text="Open password bank", command=open_password_bank_window)
    def open_create_master_password_window():
        win = tk.Toplevel(root)
        # keep the window on top even when messageboxes are shown
        win.attributes("-topmost", True)
        if master_password:
            messagebox.showinfo(
                "Info",
                ("A master password has already "
                 "been created. Please enter it.")
            )
            win.destroy()
        else:
            master_password_label = tk.Label(win, text="Master password:")
            master_password_entry = tk.Entry(win, show="•")
            master_password_entry.focus()
            master_password_confirm_label = tk.Label(win, text="Confirm master password:")
            master_password_confirm_entry = tk.Entry(win, show="•")
            def submit_master_password():
                mp = master_password_entry.get()
                if not mp:
                    messagebox.showerror("Error", "Master password is empty")
                else:
                    mp_confirm = master_password_confirm_entry.get()
                    if not mp_confirm:
                        messagebox.showerror(
                            "Error",
                            "Confirm master password is empty"
                        )
                    else:
                        if mp != mp_confirm:
                            messagebox.showerror(
                                "Error",
                                "Master password does not match confirm"
                            )
                        else:
                            nonlocal master_password
                            master_password = mp
                            win.destroy()
            submit_button = tk.Button(win, text="Submit", command=submit_master_password)
            master_password_label.pack()
            master_password_entry.pack()
            master_password_confirm_label.pack()
            master_password_confirm_entry.pack()
            submit_button.pack()
    create_button = tk.Button(root, text="First time? Create master password", command=open_create_master_password_window)
    master_password_label.pack()
    master_password_entry.pack()
    open_bank_button.pack()
    create_button.pack()
    root.mainloop()


if __name__ == '__main__':
    main()